# Physical Metallurgy ME6930

Homework and personal coding stuff lol

# Dependency

- Plotly
- Python

### Interatomic Potential/Force

![alt text](/pics/int_p.png)
![alt text](/pics/int_F.png)


### Homework 2 Problem 2

Compare the theoretically calculated thermal expansion coefficients with the experimentally observed coefficients.

Incomprehensible jibberish 

```math
 \alpha = \frac{d \epsilon_T}{dT} = \\
                         (1/2)*((Tb-T)^{(-1/3)}) * ((1/12)*((Tb + \sqrt(T*Tb))^{(-5/6)}) * ((T*Tb)^{(-1/2)}) 
                                             * Tb *((Tb - T)^{(1/6)})
                                            \\ + (1/6)*((Tb - T)^{(-5/6)}) * 
                                             ( (Tb + \sqrt(T*Tb))^{(1/6)} + (Tb - \sqrt(T*Tb))^{(1/6)})
                                            )
```

![alt text](HW2/pics/problem2.png)
